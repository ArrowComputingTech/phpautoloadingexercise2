<?php

  spl_autoload_register(function($classname) {
    $classname = str_replace("\\", DIRECTORY_SEPARATOR, $classname);
    echo "Loading the class: " . $classname . PHP_EOL;
    require_once $classname . '.php';
  });

  $loader1 = new src\loader();
  $loader1->hello();

?>
